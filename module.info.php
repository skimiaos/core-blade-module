<?php

return [
    'name'        => 'Module Blade',
    'author'      => 'Skimia',
    'description' => 'Fournit des outils pour utiliser des blocks fonctionnant dynamiquement et créer des Tags Blade simplement sans utiliser d\'expressions régulières',
    'namespace'   => 'Skimia\\Blade',
    'require'     => ['skimia.modules']
];