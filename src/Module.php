<?php

namespace Skimia\Blade;

use Blade;
use Skimia\Modules\ModuleBase;

class Module extends ModuleBase{

    public function preBoot(){
        Blade::extend(function($view, $compiler){
            $pattern = $compiler->createPlainMatcher('spaceless');
            return preg_replace($pattern, '$1<?php ob_start(); ?>$2', $view);
        });
        Blade::extend(function($view, $compiler){
            $pattern = $compiler->createPlainMatcher('endspaceless');
            return preg_replace($pattern, '$1<?php echo trim(preg_replace(\'/>\s+</\', \'><\', ob_get_clean())); ?>$2', $view);
        });

        \BladeHelper::createFunction('block','BladeBlock::start','<?php echo BladeBlock::set($1,'."\n".'function($temp__vars){extract($temp__vars); ?>');

        \BladeHelper::createTag('endoverride','<?php }, array_except(get_defined_vars(), array(\'__data\', \'__path\')),false,true); ?>');
        \BladeHelper::createTag('endshow','<?php }, array_except(get_defined_vars(), array(\'__data\', \'__path\')),true,false); ?>');
        \BladeHelper::createTag('endblock','<?php }, array_except(get_defined_vars(), array(\'__data\', \'__path\')),false,false); ?>');
    }

    public function register(){
        parent::register();
    }

    public function getAliases(){
        return [
            'BladeHelper' => 'Skimia\Blade\Facades\BladeHelper',
            'BladeBlock' => 'Skimia\Blade\Facades\BladeBlock',
        ];
    }
} 