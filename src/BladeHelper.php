<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 23/11/2014
 * Time: 09:16
 */

namespace Skimia\Blade;
use VerbalExpressions;
use Blade;

class BladeHelper {

    public function __construct(){
        require_once module_lib('skimia.blade','verbal-expr','VerbalExpression.php');
    }

    /**
     * Crée un tag simple de remplacement $replacement html injecté a la place (peut contenir <?php /code php/ ?>)
     * @$tag => $replacement
     * @example "BladeHelper::createTag('View','<div ui-view></div>');//remplace @View par <div ui-view></div>" Utilisation Simple.
     * @param $tag @$tag
     * @param $replacement code de remplacement du tag
     */
    public function createTag($tag, $replacement){

        Blade::extend(function($view) use($tag,$replacement){

            $regex = new VerbalExpressions;
            return $regex->then('@'.$tag)->withAnyCase(true)->addModifier('g')->replace($view,$replacement);

        });

    }

    /**
     * Crée un appel de fonction
     * $call appel de n'importe quel code php
     * $replace code de remplacement où "$1" sont les paramètres
     * @$tag(/paramètres/) => <?php echo $call(/paramètres/); ?>
     * @example "BladeHelper::createFunction('State','AngularHelper::State');//remplace @State(params) par <?php echo AngularHelper::State(params); ?>"
     * @param $tag @$tag(/paramètres/)
     * @param $call
     * @param bool $replace replace directement le code $1 vaut les paramètres
     */
    public function createFunction($tag,$call,$replace = false){
        Blade::extend(function($view) use($tag,$call,$replace) {

            $regex = new VerbalExpressions;
            return $regex->then('@' . $tag)->withAnyCase(true)->addModifier('g')->add('[ ]*')->then('(')->add('(.*)')->then(')')->replace($view, $replace !== false? $replace:'<?php echo ' . $call . '($1); ?>');

        });
    }

    /**
     * crée un simple remplacement
     * $atribut html (ex:ng-click)
     * $call appel javascript (ex:$state.go)
     * $after exemple pour jquery
     * ($after='slideUp()') (@slideUp("css selector")
     * => onclick="$("css selector").slideUp()"
     * @$tag(/paramètres/) => $attribute="$call(/params/)[.$after]"
     * @example "BladeHelper::createCall('State','ng-click','$state.go');//remplace @State(params) par ng-click=$state.go(parametres)"
     * @param $tag
     * @param $attribute
     * @param $call
     * @param bool $after par exemple un selector .addClass('ta classe css')
     */
    public function createJsCall($tag,$attribute,$call, $after=false){
        Blade::extend(function($view) use($tag,$attribute,$call, $after) {

            $regex = new VerbalExpressions;
            return $regex->then('@' . $tag)->addModifier('g')->add('[ ]*')->then('(')->add('(.*)')->then(')')->replace($view, $attribute.'="'.$call .'($1)'.$after !==false ?'.'.$after:''.'"');

        });
    }

    protected $closures = [];

    public function createClosureTag($tag,$closure){
        $this->closures[$tag] = $closure;

        $this->createTag($tag,'<?php echo BladeHelper::getClosureTag(\''.$tag.'\')?>');

    }

    public function createClosureFunctionTag($tag,$closure){
        $this->closures[$tag] = $closure;

        $this->createFunction($tag,null,'<?php echo BladeHelper::getClosureTag(\''.$tag.'\', [$1])?>');

    }
    public function createClosureFunctionTagWithParentVars($tag,$closure,$parents = []){
        $this->closures[$tag] = $closure;
        $arr = '';
        foreach ($parents as $var) {
            $arr .= sprintf('(isset($%s) ? $%s : null) ,',$var,$var);
        }

        $arr = trim($arr,',');


        $this->createFunction($tag,null,'<?php echo BladeHelper::getClosureTagArray(\''.$tag.'\', ['.$arr.(count($parents > 0) ? ',':'').'$1])?>');

    }

    public function getClosureTag($tag,$options = []){
        return $this->closures[$tag]($options);
    }

    public function getClosureTagArray($tag,$options = []){
        return call_user_func_array($this->closures[$tag],$options);
    }
} 