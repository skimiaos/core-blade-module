<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 23/11/2014
 * Time: 09:28
 */

namespace Skimia\Blade\Facades;

use \Illuminate\Support\Facades\Facade;

class BladeBlock extends Facade{

    protected static function getFacadeAccessor(){
        return 'Skimia\Blade\BladeBlock';
    }
} 