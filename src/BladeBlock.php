<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 04/01/2015
 * Time: 16:15
 */

namespace Skimia\Blade;


class BladeBlock {

    /**
     * All of the finished, captured sections.
     *
     * @var array
     */
    protected $blocks = array();


    public function set($block, $content_closure, $vars, $show = false, $override=false)
    {
        if (isset($this->blocks[$block]) && $override == true)
        {
            $this->blocks[$block] = $content_closure;
        }elseif(!isset($this->blocks[$block]))
            $this->blocks[$block] = $content_closure;

        if($show){
            $closure = $this->blocks[$block];
            return $closure($vars);
        }

    }

    public function reset(){
        $this->blocks = [];

        return $this;
    }

}